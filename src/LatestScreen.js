import React from 'react';
import { Link } from 'react-router-dom';

import './components/header';
import Header from './components/header';
import Footer from './components/footer';

import './styles/press.css';

class LatestScreen extends React.Component {

    constructor(props) {
        super(props);
        this.slides =  [{
            "image":"/latest-news/01-earle-cabbell-building-2.jpg",
            "title":"JusticeText rolls out cloud-based video evidence management platform for the Federal Public Defender for the Northern District of Texas",
            "text": "JusticeText is proud to announce our latest partnership with the Federal Public Defender for the Northern District of Texas, one of the largest federal defender organizations in the country."
        },{
            "image":"/latest-news/02-arlington-courthouse.jpg",
            "title":"JusticeText secures statewide contract with the Virginia Indigent Defense Commission",
            "text":"JusticeText is pleased to announce our annual contract with the Virginia Indigent Defense Commission (VIDC) to roll out our platform within 9 offices across the state over the next year."
        },{
            "image":"/latest-news/03-camelback-icon.png",
            "title":"JusticeText secures $40K investment from Camelback Ventures",
            "text":"Our CEO Devshi Mehrotra was selected as one of eleven Camelback 2021 Fellows for her leadership at JusticeText."
        },{
            "image":"/latest-news/04-ssr-featured-image-1.jpg",
            "title":"JusticeText expands to Tulsa with latest partnership with Still She Rises",
            "text":"JusticeText is excited to announce our newest partnership with Still She Rises, a holistic defense organization based in Tulsa, Oklahoma that caters to the unique needs of justice-involved women."
        },{
            "image":"/latest-news/05-photo-2.png",
            "title":"Reflection: Our experience attending the 2021 NACDL annual conference",
            "text":"Our team attended the annual National Association of Criminal Defense Lawyers (NACDL) conference in Milwaukee."
        },{
            "image":"/latest-news/06-nacdl-og.png",
            "title":"JusticeText is selected as an official NACDL Affinity Partner",
            "text":"JusticeText has been selected as an official Affinity Partner for the National Association of Criminal Defense Lawyers (NACDL)."
        },{
            "image":"/latest-news/07-wichita.jpg",
            "title":"JusticeText announces partnership with the Office of the Federal Public Defender in Kansas",
            "text":"JusticeText is excited to announce our latest partnership with the Office of the Federal Public Defender in the District of Kansas."
        },{
            "image":"/latest-news/08-022719crj-gjp-022.jpg",
            "title":"JusticeText announces collaboration with the Georgia Justice Project",
            "text":"JusticeText is pleased to announce that we will be partnering with the Georgia Justice Project (GJP) to support their criminal defense team provide legal representation across Fulton and DeKalb County."
        },{
            "image":"/latest-news/09-bodycam.jpg",
            "title":"A look at how audio and video evidence is transforming public defense in the 21st century",
            "text":"To better understand the impact of digital discovery upon public defense offices, we asked 109 participants in our pilot programs about their experiences managing these new sources of evidence."
        },{
            "image":"/latest-news/10-dave-johnson.jpg",
            "title":"Dave Johnson's vision for progressive change in Virginia's criminal justice system",
            "text":"Dave Johnson is Executive Director of the Virginia Indigent Defense Commission, the agency responsible for overseeing the state's public defense system, which comprises more than 30 offices."
        },{
            "image":"/latest-news/11-lanitria-turner+full2.jpg",
            "title":"LaNitria Turner on defending indigent women and girls in Tulsa, Oklahoma",
            "text":"LaNitria Turner is an Investigation Practice Supervisor at Still She Rises, a holistic legal aid organization dedicated exclusively to the defense of mothers in the criminal justice system."
        }];
    
    }

    render() {
        return (
            <div id="LatestScreen">
                <Header></Header>
                <div className="container">

                    <div className="common-title">
                        <h1>Latest News</h1>
                        <p>Stay up to date on our growth, partnerships, and product development.</p>
                    </div>
                    
                    <div className="container media-press">
                        <div className="common-columns max-2">
                            {this.slides.map((slide, indexSlide) => {
                                return (
                                    <div className="box">
                                        <div className="press medium-press">
                                            <div className="image">
                                                <img src={slide.image} alt={slide.title} />
                                            </div>
                                            <div className="content">
                                                <h2 className="title">{slide.title}</h2>
                                                <p className="text">{slide.text}</p>
                                                <Link className="btn" to={"#"}>View more</Link>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    
                </div>
                <Footer></Footer>
            </div>
        );
    }

}

export default LatestScreen;