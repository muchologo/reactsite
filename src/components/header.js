import React from "react";
import { Link, withRouter } from 'react-router-dom';
import '../styles/component-header.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.sections = [{
            "url":"about-us",
            "title":"About Us"
        },{
            "url":"case-studies",
            "title":"Case Studies",
        },{
            "url":"press",
            "title":"Press"
        },{
            "url":"latest",
            "title":"Latest"
        }];
    }

    render() {
        
        const currentSection  = this.props.location.pathname.split("/").join("");
    
        return (
            <header>
                <div className="container">
                    <div className="logo">
                        <Link to={'/'}><span>JusticeText</span></Link>
                    </div>
                    <div className="links">
                        {this.sections.map((section, sectionIndex) => {
                            const active = (currentSection === section.url ? "active" : "");
                            return (
                                <Link to={"/"+section.url} className={active}>{section.title}</Link>
                            );
                        })}
                        <Link to={"/early-access"} className="btn">Get early access</Link>
                    </div>
                    <Link to={"/about-us"} className="bars"><FontAwesomeIcon icon={["fal", "bars"]} fixedWidth className="icon" /></Link>
                </div>
            </header>
        );
    }

}

export default withRouter(Header);