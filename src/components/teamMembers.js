import React from "react";
import '../styles/component-team-members.css';

class TeamMembers extends React.Component {

    render() {
        return (
            <div className="common-columns team-members">
                <div className="box">
                    <div className="team-member">
                        <img src={"/devshi-mehrotra.png"} alt="Devshi Mehrotra" />
                        <h2>Devshi Mehrotra</h2>
                        <span className="role">CEO</span>
                        <p>Devshi is a Schwarzman Scholar and Obama Foundation Community Leader recognized for her work at the intersection of tech and impact.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
                <div className="box">
                    <div className="team-member">
                        <img src={"/leslie-jones-dove.png"} alt="Leslie Jones-Dove" />
                        <h2>Leslie Jones-Dove</h2>
                        <span className="role">CTO</span>
                        <p>Leslie is a full-stack engineer with extensive technical experience scaling web products across Google and JP Morgan.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
                <div className="box">
                    <div className="team-member">
                        <img src={"/jaime-zaballa.png"} alt="Jaime Zaballa" />
                        <h2>Jaime Zaballa</h2>
                        <span className="role">Engineer</span>
                        <p>Jaime is an full stack engineer wizard from Mexico who loves working with fast-paced and mission-driven startup teams.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
                <div className="box">
                    <div className="team-member">
                        <img src={"/oscar-hull.png"} alt="Oscar Hull" />
                        <h2>Oscar Hull</h2>
                        <span className="role">Engineer</span>
                        <p>Oscar is an engineering wizard from Mexico who loves working with fast-paced and mission-driven startup teams.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
                <div className="box">
                    <div className="team-member">
                        <img src={"/caroline-hanson.png"} alt="Caroline Hanson" />
                        <h2>Caroline Hanson</h2>
                        <span className="role">Intern</span>
                        <p>Caroline is a fourth year at the University of Chicago studying Public Policy and Computer Science with a passion for criminal justice reform.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>

                <div className="box">
                    <div className="team-member">
                        <img src={"/solana-adedokun.png"} alt="Solana Adedokun" />
                        <h2>Solana Adedokun</h2>
                        <span className="role">Intern</span>
                        <p>Solana is a Law, Letters, and Society student at the University of Chicago who, as a journalist, wants to know the stories behind people involved in a case and how their stories can help guide us to a fair solution.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
                <div className="box">
                    <div className="team-member">
                        <img src={"/lily-ehsani.png"} alt="Lily Ehsani" />
                        <h2>Lily Ehsani</h2>
                        <span className="role">Intern</span>
                        <p>Lily is a third year at the University of Chicago studying computer science and is passionate about improving the quality of help provided to low-income people by public institutions.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
                <div className="box">
                    <div className="team-member">
                        <img src={"/nick-clifford.png"} alt="Nick Clifford" />
                        <h2>Nick Clifford</h2>
                        <span className="role">Intern</span>
                        <p>Nick is a computer science student at UChicago who is passionate about programming for social impact.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
                <div className="box">
                    <div className="team-member">
                        <img src={"/frederick-zhu.png"} alt="Frederick Zhu" />
                        <h2>Frederick Zhu</h2>
                        <span className="role">Intern</span>
                        <p>Frederick is a computer science student at UChicago with a rich background in social science who trusts in the power of technology on social welfare.</p>
                        <div className="social">
                            <a href={"https://twitter.com/ceo"}><span>Twitter</span></a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default TeamMembers;