import React from "react";
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import '../styles/component-footer.css';

class Footer extends React.Component {

    render() {
        return (
            <footer>
                <div className="container">
                    <div className="legal">
                        <p>&copy; 2021 All rights reserved.</p>
                        <Link to={"/"}>Terms of Service</Link> and <Link to={"/"}>Privacy Policy</Link>
                    </div>
                    <div className="social">
                        <Link to={"/"}><FontAwesomeIcon icon={["fab", "facebook"]} fixedWidth className="icon" /></Link>
                        <Link to={"/"}><FontAwesomeIcon icon={["fab", "twitter"]} fixedWidth className="icon" /></Link>
                        <Link to={"/"}><FontAwesomeIcon icon={["fab", "youtube"]} fixedWidth className="icon" /></Link>
                        <Link to={"/"}><FontAwesomeIcon icon={["fab", "linkedin"]} fixedWidth className="icon" /></Link>
                    </div>
                </div>
            </footer>
        );
    }

}

export default Footer;