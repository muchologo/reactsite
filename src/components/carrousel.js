import React from 'react';
import DOMPurify from 'dompurify'

import '../styles/component-carrousel.css';
import Loader from './loader';

class Carrousel extends React.Component {

    state = {
        "currentItem": 1,
        "slides": []
    }

    componentDidMount() {
        this.setState({"slides": this.props.slides});
    }

    go(index) {
        this.setState({"currentItem":index});
    }

    prev() {
        const newIndex = (this.state.currentItem === 1 ? this.state.slides.length : this.state.currentItem - 1);
        this.go(newIndex);
    }

    next() {
        const newIndex = (this.state.currentItem === this.state.slides.length ? 1 : this.state.currentItem + 1);
        this.go(newIndex);
    }

    __withContent() {
        return (
            <div id="carrousel" style={{"backgroundColor":this.state.currentColor}}>
                <div className="container">
                    <button onClick={() => { this.prev(); }} className="previous">Previous</button>
                    <button onClick={() => { this.next(); }} className="next">Next</button>
                    <div className="bullets">
                        {this.state.slides.map((slide, index) => {
                            const indexBullet = index + 1;
                            const keyBullet = 'bullet' + indexBullet;
                            const classBullet = (indexBullet === this.state.currentItem ? " active" : "");
                            return (<button key={keyBullet} className={"bullet" + classBullet} onClick={() => { this.go(indexBullet); }}>{indexBullet}</button>)
                        })}
                    </div>
                    <div className="slides">
                        {this.state.slides.map((slide, index) => {
                            const indexPicture = index + 1;
                            const keyPicture = 'slide' + indexPicture;
                            const classPicture = (indexPicture === this.state.currentItem ? " active" : "");
                            return (
                                <div key={keyPicture} className={"slide" + classPicture}>
                                    <div className="image">
                                        <img src={slide.image} alt={slide.title} />
                                    </div>
                                    <div className="content">
                                        <span className="logo"><img src={slide.logo} alt={slide.logo} /></span>
                                        <h2 className="title">{slide.title}</h2>
                                        <div className="text" dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(slide.text)}} />
                                        <p className="author">{slide.author}</p>
                                        <a className="btn" href={slide.url}>Learn more</a>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }

    __withoutContent() {
        return (
            <div id="carrousel" className="loading">
                <Loader outerColor={"green"} innerColor={"green"}></Loader>
            </div>
        )
    }

    render() {
        const hasContent = (this.state.slides.length > 0);
        const component = hasContent ? this.__withContent() : this.__withoutContent();
        return (component);
    }

}

export default Carrousel;