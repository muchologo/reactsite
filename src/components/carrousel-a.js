import React from 'react';
import axios from 'axios';

import '../styles/component-carrousel.css';
import Loader from './loader';

class Carrousel extends React.Component {

    state = {
        "currentItem": 1,
        "currentColor": "black",
        "slides": []
    }

    constructor(props) {
        super(props);
        this.endpoint = 'https://api.unsplash.com/photos';
        this.accessKey = 'c3Oc-oh-_xWyDJoIhiI4Vmrkuo3iBWZUDSkpu1rHqr4';
        this.secretKey = 'kbhPvvuTLEMOpzHDhvF2LRjhjmFCUm8qXvSFYrKmKj4';
    }

    componentDidMount() {
        this.loadImages();
    }

    loadImages() {
        axios.get(this.endpoint, {
            "params":{
                "client_id":this.accessKey,
                "page":1,
                "per_page":5,
                "order_by":"popular"
            }
        }).then((result) => {
            if (result.data) {
                const slides = result.data.map((item) => {
                    return {
                        "url":item.urls.regular,
                        "thumb": item.urls.thumb,
                        "description":item.alt_description||"This image does not have a description",
                        "user":item.user.first_name,
                        "color":item.color
                    };
                });
                this.setState({"slides":slides}, () => {
                    // Start the first slide
                    this.go(this.state.currentItem);
                });
            }
        }).catch((error) => {
            console.log("error", error);
        });
    }

    go(index) {
        const newColor = this.state.slides[index-1].color;
        this.setState({"currentItem":index, "currentColor":newColor});
    }

    prev() {
        const newIndex = (this.state.currentItem === 1 ? this.state.slides.length : this.state.currentItem - 1);
        this.go(newIndex);
    }

    next() {
        const newIndex = (this.state.currentItem === this.state.slides.length ? 1 : this.state.currentItem + 1);
        this.go(newIndex);
    }

    __withContent() {
        return (
            <div id="carrousel" style={{"backgroundColor":this.state.currentColor}}>
                <div className="container">
                    <button onClick={() => { this.prev(); }} className="previous">Previous</button>
                    <button onClick={() => { this.next(); }} className="next">Next</button>
                    <div className="bullets">
                        {this.state.slides.map((slide, index) => {
                            const indexBullet = index + 1;
                            const keyBullet = 'bullet' + indexBullet;
                            const classBullet = (indexBullet === this.state.currentItem ? " active" : "");
                            return (<button key={keyBullet} className={"bullet" + classBullet} onClick={() => { this.go(indexBullet); }}>{indexBullet}</button>)
                        })}
                    </div>
                    <div className="slides">
                        {this.state.slides.map((slide, index) => {
                            const indexPicture = index + 1;
                            const keyPicture = 'slide' + indexPicture;
                            const classPicture = (indexPicture === this.state.currentItem ? " active" : "");
                            return (
                                <div key={keyPicture} className={"slide" + classPicture}>
                                    <div className="image">
                                        <img src={slide.thumb} alt={slide.thumb}></img>
                                    </div>
                                    <div className="content">
                                        <h2>{slide.user}</h2>
                                        <p>{slide.description}</p>
                                        <a href={slide.url}>Big Image</a>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }

    __withoutContent() {
        return (
            <div id="carrousel" className="loading">
                <Loader outerColor={"green"} innerColor={"green"}></Loader>
            </div>
        )
    }

    render() {
        const hasContent = (this.state.slides.length > 0);
        const component = hasContent ? this.__withContent() : this.__withoutContent();
        return (component);
    }

}

export default Carrousel;