import React from "react";
import '../styles/component-loader.css';

class Loader extends React.Component {

    render() {

        let outerStyle = {};
        if (this.props.outerColor !== undefined && this.props.outerColor !== null) {
            outerStyle["borderTopColor"] = this.props.outerColor;
        }

        let innerStyle = {};
        if (this.props.innerColor !== undefined && this.props.innerColor !== null) {
            innerStyle["borderColor"] = this.props.innerColor;
        }
        
        return (
            <div className="loader">
                <div className="outer" style={outerStyle}></div>
                <div className="inner" style={innerStyle}></div>
            </div>
        );
    }

}

export default Loader;