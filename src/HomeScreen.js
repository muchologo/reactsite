import React from 'react';

import './components/header';
import Header from './components/header';
import Footer from './components/footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './styles/responsive.css';
import './styles/articles.css';
import './styles/evidences.css';
import './styles/timeline-events.css';
import './styles/features.css';
import './styles/partners.css';
import './styles/cases.css';
import './styles/supporters.css';

class HomeScreen extends React.Component {

    render() {
        return (
            <div id="HomeScreen">
                <Header />
                
                <div className="container">
                    <div className="highlight-image">
                        <div className="box">
                            <div className="content">
                                <h1>Video evidence is a powerful vehicle for justice.</h1>
                                <p>We are strengthening the ability of public defenders to analyze this crucial data with our evidence management software. </p>
                                <a className="btn" href={"https://blog.codingitforward.com/justicetext-evidence-management-platform-for-fairer-criminal-justice-outcomes-3478e7243ba1"}>Get early access</a>
                                <a className="btn" href={"https://blog.codingitforward.com/justicetext-evidence-management-platform-for-fairer-criminal-justice-outcomes-3478e7243ba1"}>Get in touch today</a>
                            </div>
                        </div>
                        <div className="box">
                            <div className="image">
                                <img src="/undraw-typewriter.svg" alt="Foo" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container articles">
                    <div className="common-columns">
                        <div className="box">
                            <div className="article"><img src="/articles/law360.png" alt="law360" /></div>
                        </div>
                        <div className="box">
                            <div className="article"><img src="/articles/forbes.png" alt="forbes" /></div>
                        </div>
                        <div className="box">
                            <div className="article"><img src="/articles/attorney-at-law-magazine.png" alt="attorney at law magazine" /></div>
                        </div>
                        <div className="box">
                            <div className="article"><img src="/articles/aba-journal.png" alt="aba journal" /></div>
                        </div>
                    </div>
                </div>

                <div className="container evidences">
                    <div className="common-title">
                        <h1>Use of Digital Evidence is Exploding</h1>
                        <p>Video evidence can help ensure accountability and transparency within the criminal justice system, but this is only possible if the data is:</p>
                    </div>
                    <div className="common-columns">
                        <div className="box">
                            <div className="evidence">
                                <FontAwesomeIcon icon={["fal", "search"]} fixedWidth className="icon" />
                                <h2>Searchable</h2>
                                <p>Search for keywords across hundred of hours</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="evidence">
                                <FontAwesomeIcon icon={["fal", "tags"]} fixedWidth className="icon" />
                                <h2>Taggable</h2>
                                <p>Use cuztomizable tags to catalog discovery</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="evidence">
                                <FontAwesomeIcon icon={["fal", "comment-alt-smile"]} fixedWidth className="icon" />
                                <h2>Digestible</h2>
                                <p>Take notes and create video clips with ease</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="evidence">
                                <FontAwesomeIcon icon={["fal", "mail-bulk"]} fixedWidth className="icon" />
                                <h2>Shareable</h2>
                                <p>Share your data with colleagues and clients</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container timeline">
                    <div className="common-title">
                        <h2>How It Works</h2>
                    </div>
                    <div className="events">
                        <div className="event">
                            <div className="stamp">
                                <span className="number">1</span>
                            </div>
                            <div className="content">
                                <h3>Upload audiovisual evidence</h3>
                                <p>Upload body camera footage, interrogation videos, courtroom proceedings, and jail calls up to four hours in length to our online platform.</p>
                            </div>
                            <div className="image">
                                <img src="/events/upload-audiovisual-evidence.png" alt="Upload audiovisual evidence" />
                            </div>
                        </div>
                        <div className="event">
                            <div className="stamp">
                                <span className="number">2</span>
                            </div>
                            <div className="content">
                                <h3>Generate an automated transcript</h3>
                                <p>Our service generates an automated transcription of the uploaded evidence, notifying you immediately upon completion.</p>
                            </div>
                            <div className="image">
                                <img src="/events/generate-an-automated-transcript.svg" alt="Generate an automated transcript" />
                            </div>
                        </div>
                        <div className="event">
                            <div className="stamp">
                                <span className="number">3</span>
                            </div>
                            <div className="content">
                                <h3>Edit, analyze, and export the transcript</h3>
                                <p>Use our interactive editor to make edits, take notes, create video clips, and export the generated transcript.</p>
                            </div>
                            <div className="image">
                                <img src="/events/edit-analyze-and-export-the-transcript.svg" alt="Edit, analyze, and export the transcript" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container features">
                    <div className="common-title">
                        <h2>Product Features</h2>
                        <p>The JusticeText platform is specifically designed to expedite the pre-trial preparation experience.</p>
                    </div>
                    <div className="common-columns">
                        <div className="box">
                            <div className="feature">
                                <span className="icon"><FontAwesomeIcon icon={["fal", "bullhorn"]} fixedWidth className="icon" /></span>
                                <h3>Speaker recognition</h3>
                                <p>We are able to automatically identify when different individuals are speaking in the video.</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="feature">
                                <span className="icon"><FontAwesomeIcon icon={["fal", "fire-alt"]} fixedWidth className="icon" /></span>
                                <h3>Accuracy heat map</h3>
                                <p>The application indicates which transcribed phrases have low confidence scores to assist with the editing process.</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="feature">
                                <span className="icon"><FontAwesomeIcon icon={["fal", "stopwatch-20"]} fixedWidth className="icon" /></span>
                                <h3>Timestamped notes</h3>
                                <p>Each note entry is timestamped to correspond to a specific point in the video.</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="feature">
                                <span className="icon"><FontAwesomeIcon icon={["fal", "cloud-download"]} fixedWidth className="icon" /></span>
                                <h3>Export file</h3>
                                <p>Easily export the transcript as a well-formatted Word Document or video clips in MP4 format for later use.</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="feature">
                                <span className="icon"><FontAwesomeIcon icon={["fal", "film"]} fixedWidth /></span>
                                <h3>Video clipping</h3>
                                <p>Generate clips of video evidence for use in trial by highlighting the relevant section of the transcript.</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="feature">
                                <span className="icon"><FontAwesomeIcon icon={["fal", "globe-americas"]} fixedWidth /></span>
                                <h3>Multi-language support</h3>
                                <p>Our platform supports 30+ different languages, including Spanish, Mandarin, and German.</p>
                            </div>
                        </div>
                    </div>
                    <div className="common-title highlight-text">
                        <span className="icon"><FontAwesomeIcon icon={["fal", "hands-helping"]} fixedWidth /></span>
                        <blockquote>Over 80% of criminal cases involve video evidence. But the vast majority of public defenders struggle to find time to review all the footage.</blockquote>
                        <h2>We are working to change that.</h2>
                    </div>
                </div>

                <div className="container partners">
                    <div className="common-title">
                        <h1>Our Partners</h1>
                        <p>We work with indigent defense attorneys nationwide.</p>
                    </div>
                    <div className="common-columns">
                        <div className="box">
                            <div className="partner"><img src="/partners/queens-defenders.jpg" alt="queens-defenders" /></div>
                        </div>
                        <div className="box">
                            <div className="partner"><img src="/partners/hamilton-county-defender.png" alt="hamilton-county-defender" /></div>
                        </div>
                        <div className="box">
                            <div className="partner"><img src="/partners/nycds.jpg" alt="nycds" /></div>
                        </div>
                        <div className="box">
                            <div className="partner"><img src="/partners/alameda-country-public-defenders.jpg" alt="alameda-country-public-defenders" /></div>
                        </div>
                        <div className="box">
                            <div className="partner"><img src="/partners/trla.png" alt="trla" /></div>
                        </div>
                        <div className="box">
                            <div className="partner"><img src="/partners/nacdl.png" alt="nacdl" /></div>
                        </div>
                        <div className="box">
                            <div className="partner"><img src="/partners/pdsdc.png" alt="pdsdc" /></div>
                        </div>
                    </div>
                </div>

                <div className="container cases">
                    <div className="common-title">
                        <h1>What People Say About Us</h1>
                        <p>We take our users' feedback very seriously and building our product right alongside our early champions.</p>
                    </div>
                    <div className="common-columns max-2">
                        <div className="box">
                            <div className="case">
                                <img src="/cases/queens.png" alt="Queens" />
                                <blockquote>"It's so easy to reference all my discovery and just keep it in front of me, as opposed to the old process which was unnecessarily cumbersome. Using JusticeText is a huge cut down of unnecessary logistical barriers to preparing your case."</blockquote>
                                <cite>Nick Justiz, Staff Attorney</cite>
                            </div>
                        </div>
                        <div className="box">
                            <div className="case">
                                <img src="/cases/kellie-mannette.png" alt="Kellie mannette" />
                                <blockquote>“JusticeText minimizes the time I need to spend going back through videos and transcripts. The software does a really good job of capturing the content of the videos. In fact, I’ll often grab quotes from the transcripts to put into motions that I’m preparing.”</blockquote>
                                <cite>Kellie Mannette, Criminal Defense Attorney</cite>
                            </div>
                        </div>
                        <div className="box">
                            <div className="case">
                                <img src="/cases/georgia-justice-project.png" alt="georgia justice project" />
                                <blockquote>"It allows us to do better work and more work for each of our clients because we don't have to focus so much on listening to videos and jail calls for hours at a time. We have a case with well over 100 jail calls, and using JusticeText has given us a way to decide which ones are important more quickly."</blockquote>
                                <cite>Rachel Holmes, Managing Attorney</cite>
                            </div>
                        </div>
                        <div className="box">
                            <div className="case">
                                <img src="/cases/still-she-rises-tulsa.jpg" alt="still she rises tulsa" />
                                <blockquote>"Access to police body camera is a struggle in and of itself. But when you do have it, it’s gold. If I don’t transcribe a body camera, I’ve seen situations in which someone might misstate or misquote what actually happened. That’s why JusticeText is a really incredible tool. "</blockquote>
                                <cite>LANITRIA TURNER, INVESTIGATOR</cite>
                            </div>
                        </div>
                        <div className="box">
                            <div className="case">
                                <img src="/cases/federal-public-defender-district-of-kansas.png" alt="Federal public defender district of kansas" />
                                <blockquote>"I like the speed with which we can review body cam and pole cam recordings. It gives us the ability to do a word search and jump to that part of the video so quickly and to be able to clip sections for presentations in the office. JusticeText is intuitive and easy to use."</blockquote>
                                <cite>Melody Brannon, Federal Public Defender</cite>
                            </div>
                        </div>
                        <div className="box">
                            <div className="case">
                                <img src="/cases/minn-bopd.png" alt="Minn bopd" />
                                <blockquote>“The program was super helpful for me to make notes every time something caught my eye that I wanted to jump back into or make a clip of. I know this is going to be extremely helpful for trial prep coming up for a homicide we have where there are endless bodycams and witness statements.”</blockquote>
                                <cite>Hayley Albright, Investigator</cite>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="lowlight-text">
                    <div className="container">
                        <div className="common-title">
                            <h2>Ready to get started?</h2>
                            <p>The JusticeText team is building our software in direct collaboration with indigent defense attorneys, tailoring our product to our users' unique set of needs and challenges. If you are interested in learning more about our services, please reach out to <a href="mailto:team@justicetext.com">team@justicetext.com</a> or schedule a demo.</p>
                            <a className="btn">Schedule Demo</a>
                        </div>
                    </div>
                </div>

                <div className="container supporters">
                    <div className="common-title">
                        <h1>Our Supporters</h1>
                    </div>
                    <div className="common-columns">
                        <div className="box">
                            <div className="supporter"><img src="/supporters/duke-center-on-law-and-tech.png" alt="duke center on law and tech" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/chicago-booth-rustandy-center.jpg" alt="chicago booth rustandy center" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/coding-it-forward.png" alt="coding it forward" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/envision.jpg" alt="envision" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/the-university-of-chicago-institute-of-politics.jpg" alt="the university of chicago institute of politics" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/lexis-nexis.png" alt="lexis nexis" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/500-startups.png" alt="500 startups" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/stand-together-ventures-lab.png" alt="stand together ventures lab" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/techstars.png" alt="techstars" /></div>
                        </div>
                        <div className="box">
                            <div className="supporter"><img src="/supporters/camelback-ventures.png" alt="camelback ventures" /></div>
                        </div>
                    </div>
                </div>
                
                <Footer />
            </div>
        );
    }

}

export default HomeScreen;