import React from 'react';

import './components/header';
import Header from './components/header';
import Footer from './components/footer';
import TeamMembers from './components/teamMembers';

class AboutUsScreen extends React.Component {

    render() {
        return (
            <div id="AboutUsScreen">
                <Header></Header>
                <div className="container">

                    <div className="common-title">
                        <h1>Our Team</h1>
                        <p>We are building a team of empathetic and kind technologists with a commitment to racial and economic justice.</p>
                    </div>
                    
                    <TeamMembers />
                    
                    <div className="highlight-image">
                        <div className="box">
                            <div className="content">
                                <h2>Why We Are Building JusticeText</h2>
                                <p>Our founding team began attending college in Chicago in 2015. During the November of our freshman year, a dash cam video was released revealing that a 17-year-old, unarmed African American boy by the name of Laquan McDonald was shot 16 times by a Chicago police officer. In the aftermath of this tragedy, the city saw a proliferation of facial recognition software, gang databases, and predictive policing tools - all applications of technology designed to expedite arrest and incarceration.</p>
                                <p>In our increasingly technology-dependent criminal justice system, there was an utter lack of technological solutions built with empathy for the communities most directly affected by it. And that is exactly why we started building JusticeText.</p>
                                <p>As technologists of color, our work is grounded in both a sociological and historical appreciation for the complexities of the American criminal justice system—with its gaping faults and inequality but also its capacity for reform. In building this organization, we are committed to strengthening the capacity of our public institutions to ensure criminal legal representation for all Americans, regardless of income.</p>
                                <a href={"https://blog.codingitforward.com/justicetext-evidence-management-platform-for-fairer-criminal-justice-outcomes-3478e7243ba1"}>Read more</a>
                            </div>
                        </div>
                        <div className="box">
                            <div className="image">
                                <img src="/undraw-black-lives-matter-rndk.png" alt="Black lives matter" />
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }

}

export default AboutUsScreen;