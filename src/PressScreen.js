import React from 'react';
import { Link } from 'react-router-dom';

import './components/header';
import Header from './components/header';
import Footer from './components/footer';

import './styles/press.css';

class PressScreen extends React.Component {

    constructor(props) {
        super(props);
        this.slides =  [{
            "image":"/press/01-preview.jpg",
            "title":"Equipping public defenders with a tech solution to improve the criminal justice system",
            "text": "JusticeText CEO Devshi Mehrotra shares how leveling the playing field for public defenders is key to lasting criminal justice reform."
        },{
            "image":"/press/02-richmond_inno.jpeg",
            "title":"JusticeText is helping Virginia’s public defenders better utilize body and dash cams for court cases",
            "text":"The startup recently partnered with the Virginia Indigent Defense Commission to support 125 public defenders, its first partnership at the statewide level.",
        },{
            "image":"/press/03-camelback.png",
            "title":"They're dope. Meet the 2021 Camelback fellows.",
            "text":"We are excited to welcome our 2021 Education and Conscious Tech fellows who are joining our fellowship program that supports entrepreneurs of color and women leading innovative social impact ventures.",
        },{
            "image":"/press/04-legal_rebels.png",
            "title":"A new evidence management tool aims to help public defenders process video and audio",
            "text":"Mehrotra and Jones-Dove responded by developing a technology platform known as JusticeText, an AI-powered evidence management tool primarily geared toward public defenders."
        },{
            "image":"/press/05-preview.jpg",
            "title":"JusticeText Helps Public Defenders Better Serve Their Clients",
            "text":"JusticeText uses speech-to-text machine learning algorithms to transcribe video footage evidence, speeding up pre-trial preparation and helping overburdened and under-resourced public defenders"
        },{
            "image":"/press/06-a2j-law360.png",
            "title":"JusticeText Co-Founder On Building Tech For Public Defense",
           "text":"Public defender offices across the U.S. are under-resourced and underfunded. On top of that, the criminal cases public defenders take on can involve sifting through hours upon hours of visual and audio evidence."
        },{
            "image":"/press/07-dev-stv.png",
            "title":"A young female CEO’s bold vision to transform criminal justice",
            "text":"To celebrate Women’s History Month, we’re highlighting Devshi’s achievements as a female entrepreneur, founder, computer science expert, and criminal justice innovator."
        },{
            "image":"/press/08-500-startups.png",
            "title":"JusticeText is Helping Legal Experts With AI-Powered Evidence Tools",
            "text":"JusticeText has developed a web-based evidence management software that employs a speech-to-text machine learning algorithm to generate an interactive and searchable transcript of video and audio evidence automatically."
        },{
            "image":"/press/09-techstars-img.png",
            "title":"A New look for the 2021 Class of the Cox Enterprises Social Impact Accelerator Powered by Techstars",
            "text":"We hope that our class will be a shining example of the opportunities that exist within the social justice vertical and how to support the diverse founders that are tackling these issues."
        },{
            "image":"/press/10-chicago.png",
            "title":"21 Chicago Startups to Watch in 2021",
            "text":"On our annual Startups to Watch list, we highlight a handful of Chicago startups that are poised to make big moves in the next year, whether that's raising sizable funding rounds, acquiring other companies, accomplishing fast growth or simply continuing work on challenging problems."
        },{
            "image":"/press/11-queens-logo.png",
            "title":"Queens Defenders Partners with JusticeText to Expedite Review of Crucial Discovery",
            "text":"Queens Defenders is partnering with JusticeText, an audiovisual evidence management software, to expedite the review of body camera footage, interrogation videos, jail calls, and other crucial discovery."
        },{
            "image":"/press/12-forbes-format.png",
            "title":"Forbes 30 Under 30 2021: Social Impact",
            "text":"An audiovisual evidence management software that generates automated transcripts of body camera footage, interrogation videos, and jail calls, JusticeText expedites the pre-trial preparation time and allows public defenders to analyze crucial data."
        },{
            "image":"/press/13-ocbj-format.png",
            "title":"Irvine-Based AI Firm Aiding Public Defenders",
            "text":"Orange County’s Devshi Mehrotra, while still a student, put her computer skills to use in developing a platform that irons out some of the imbalances faced by public defenders representing financially strapped clients."
        },{
            "image":"/press/14-stvl-format.png",
            "title":"Stand Together Ventures Lab Invests in AI Solution for Public Defenders",
            "text":"Stand Together Ventures Lab (STVL3, LLC) today announced an investment in JusticeText, a software platform that makes it easier for public defenders to process and incorporate crucial audio and video evidence in defense of their clients."
        },{
            "image":"/press/15-wi-format.png",
            "title":"Companies from California to D.C. selected for gener8tor's social impact accelerator",
            "text":"Wisconsin startup accelerator gener8tor has announced the fall 2020 cohorts for its gBeta Social Impact accelerator, a free seven-week intensive program that helps social entrepreneurs refine their business model."
        },{
            "image":"/press/16-socialicon_400x400.jpg",
            "title":"Duke Law Tech Lab Demo Day grand prize goes to public defense video transcription service",
            "text":"JusticeText was awarded both the $5,000 grand prize and another $1,000 as audience favorite at Duke Law Tech Lab’s annual Demo Day pitch competition."
        },{
            "image":"/press/17-rustandy-2.png",
            "title":"Rustandy Center Announces Three 2020 Tarrson Fellows",
            "text":"New social ventures coming out of the University of Chicago are working to support emerging social entrepreneurs in Sub-Saharan Africa, expedite the pre-trial preparation experience, and connect advocacy and service organizations with individual donors and charitable foundations."
        },{
            "image":"/press/18-legal tech live.png",
            "title":"Devshi and Leslie of Criminal Justice Startup JusticeText",
            "text":"Devshi Mehrotra and Leslie Jones-Dove were graduating seniors at the University of Chicago, when they created their criminal justice reform #startup, JusticeText."
        }];
    
    }

    render() {
        return (
            <div id="PressScreen">
                <Header></Header>
                <div className="container">

                    <div className="common-title">
                        <h1>Press Coverage</h1>
                        <p>Here's what people are saying about us.</p>
                    </div>
                    
                    <div className="container media-press">
                        <div className="common-columns max-2">
                            {this.slides.map((slide, indexSlide) => {
                                return (
                                    <div className="box">
                                        <div className="press medium-press">
                                            <div className="image">
                                                <img src={slide.image} alt={slide.title} />
                                            </div>
                                            <div className="content">
                                                <h2 className="title">{slide.title}</h2>
                                                <p className="text">{slide.text}</p>
                                                <Link className="btn" to={"#"}>View more</Link>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    
                </div>
                <Footer></Footer>
            </div>
        );
    }

}

export default PressScreen;