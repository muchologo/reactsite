import React from 'react';

import Carrousel from './components/carrousel';
import Header from './components/header';
import Footer from './components/footer';

import './components/header';
import './styles/responsive.css';

class CaseStudiesScreen extends React.Component {


    constructor(props) {
        super(props);
        this.slides = [{
            "image": "studies/orange-county-courthouse.jpg",
            "logo": "cases/kellie-mannette.png",
            "title": "How a Chapel Hill attorney uses JusticeText to transcribe police interrogations in high-level criminal cases",
            "text": "<p>I'm usually adapting a tool met broadly for law firms with lots of money or the police department or the prosecutor. It's really nice to have an organization that is specifically looking at what helps us as defense attorneys for indigent defendants.&nbsp;</p><p>When you consider the course of an entire case, JusticeText minimizes the time I need to spend going back through videos and transcripts. The software does a really good job of capturing the content of the videos. In fact, I’ll often grab quotes from the transcripts to put into motions that I’m preparing.</p>",
            "author":"Kellie Mannette, Criminal Defense Attorney"
        },{
            "image": "studies/st-louis-county-courthouse-minnesota.jpg",
            "logo": "cases/minn-bopd.png",
            "title": "How JusticeText helped a Minnesota defense investigator transcribe body-worn camera footage and ensure better outcomes for her clients",
            "text": "<p>Time is of the essence in our job. If I spend too much time doing one thing, then somebody else somewhere along the line isn’t getting the help that they need.</p><p>The program was super helpful for me to make notes every time something caught my eye that I wanted to jump back into or make a clip of. I know this is going to be extremely helpful for trial prep coming up for a homicide we have where there are endless bodycams and witness statements.</p>",
            "author":"Hayley Albright, Investigator"
        },{
            "image": "studies/lincoln-county-oregon-courthouse.jpg",
            "logo": "cases/lincoln-county-oregon.png",
            "title": "How former Lincoln County District Attorney Jonathan Cable uses JusticeText to produce low-cost video transcripts for trial",
            "text": "<p>When I started 20 years ago, it was rare that you had any kind of recording. Maybe in a drug case, you would have a little tape recorder or something. But now, [there’s] the volume of body cameras and videos, which are great on one hand because people can’t lie about what happened. But it’s so much, even in a minor case, to watch, to review. It makes our jobs kind of difficult.</p><p>I had a case right before your program came out [that] I spent probably three or four days over the course of a couple of weeks just transcribing. To save that amount of time is amazing.</p>",
            "author":"Jonathan Cable, Defense Attorney"
        }]
    }

    render() {
        return (
            <div id="CaseStudiesScreen">
                <Header />
                
                <div className="lowlight-text">
                    <div className="container">
                        <div className="common-title">
                            <h1>Case Studies</h1>
                            <p>Video evidence can help ensure accountability and transparency within the criminal justice system, but this is only possible if the data is:</p>
                        </div>
                    </div>
                </div>

                <Carrousel slides={this.slides} />
                
                <Footer />
            </div>
        );
    }

}

export default CaseStudiesScreen;