import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './App.css';
import HomeScreen from './HomeScreen';
import AboutUsScreen from './AbousUsScreen';
import CaseStudiesScreen from './CaseStudiesScreen';
import PressScreen from './PressScreen';
import LatestScreen from './LatestScreen';
import EarlyAccessScreen from './EarlyAccessScreen';

/* Font awesome ================================================================================== */
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFacebook, faTwitter, faYoutube, faLinkedin } from '@fortawesome/free-brands-svg-icons';

import { 
  faSearch, faTags, faCommentAltSmile, faMailBulk,
  faBullhorn, faFireAlt, faStopwatch20, faCloudDownload, faFilm, faGlobeAmericas,
  faHandsHelping, faBars
} from '@fortawesome/pro-light-svg-icons';

library.add(
  faFacebook, faTwitter, faYoutube, faLinkedin, 
  faSearch, faTags, faCommentAltSmile, faMailBulk,
  faBullhorn, faFireAlt, faStopwatch20, faCloudDownload, faFilm, faGlobeAmericas,
  faHandsHelping, faBars
);

const App = function() {

  return (
    <div id="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={HomeScreen} />
          <Route exact path="/about-us" component={AboutUsScreen} />
          <Route exact path="/case-studies" component={CaseStudiesScreen} />
          <Route exact path="/press" component={PressScreen} />
          <Route exact path="/latest" component={LatestScreen} />
          <Route exact path="/early-access" component={EarlyAccessScreen} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
export default App;
